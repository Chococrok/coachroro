import fs from 'fs';
import path from 'path';

export default {
    srcDir: "src",
    head: {
        title: "Coach Roro",
        script: [
            { src: 'https://identity.netlify.com/v1/netlify-identity-widget.js' }
        ],
        meta: [
            { charset: "utf-8"},
            { name:"viewport", content:"width=device-width, initial-scale=1" }
        ]
    },
    buildModules: [
        '@nuxtjs/vuetify',
    ],
    server: {
        host: "0.0.0.0",
        https: process.env.NODE_ENV !== 'production' ? 
            {
                key: fs.readFileSync(path.resolve(__dirname, 'ssl', 'domain.key')),
                cert: fs.readFileSync(path.resolve(__dirname, 'ssl', 'domain.crt'))
            } : 
            undefined
    },
    generate: {
        dir: "public"
    }
};