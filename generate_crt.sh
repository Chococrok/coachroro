#!/usr/bin/env bash

var_cwd="$(dirname "$(readlink -f "$0")")"

openssl req \
  -newkey rsa:2048 -nodes \
  -keyout "$var_cwd/ssl/domain.key" -x509 -days 365 \
  -out "$var_cwd/ssl/domain.crt" \
  -subj "/C=FR/ST=Occitanie/L=Toulouse/O=roro&co/CN=moi_viking@hotmail.com"