# CoachRoro

## Requirements

  * Node.js

## Start-Up

  * After cloning the repository generate a self signed certificate by launching ```npm run generate-crt```.
  This is required because the netlifycms only works with https.
  * Run ```npm start``` and go to the https://localhost:3000

## Admin Page

  * Add ```/admin``` at then end of the URL. 
  *  The website will prompt to enter an URL you will find it on netlify.
  * **Important:** add ```https://``` at the beginning of the netlify URL